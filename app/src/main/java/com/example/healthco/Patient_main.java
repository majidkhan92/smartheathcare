package com.example.healthco;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.healthco.R;

import android.R.menu;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Patient_main extends Activity {
	
	MenuItem dr;
	String doc_id;
	//int itemPostion;
	
	/** An array of strings to populate drop down list */
	String[] actions = new String[] {
		"Patient Profile",	// patient main
		"Patient History", 	// check history 1
		"View Medications", // view medicine 2 
		"View Tests Prescribed", //view tests 3
		"Prescribe Medicine", // prescribe medicine 4
		"Prescribe Lab Test", // prescribe lab test 5
		"Conduct Preliminary Exam", //conduct exam 6
		"View Full Details", // view archive 7
	};
	
	public void onCreate(Bundle savedInstanceState){
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_main);
		
		TextView pid = (TextView)findViewById(R.id.idP);
		final String id = getIntent().getExtras().getString("patientID");
		final String dName = getIntent().getExtras().getString("Name");
		doc_id = getIntent().getExtras().getString("docID");
		pid.setText(id);
		
		

		/** Create an array adapter to populate drop down list */
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);


		/** Enabling drop down list navigation for the action bar */
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		
		
//		getActionBar().setListNavigationCallbacks(adapter, this);
//		getActionBar().setSelectedNavigationItem(position);
//		getActionBar().setSelectedNavigationItem(0);
		
		/** Defining Navigation listener */
		ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {
		
		
			
			@Override
			public boolean onNavigationItemSelected(int itemPosition, long itemId) {
				Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
				switch (itemPosition) {
//				case 0:
//                	Intent i1 = new Intent(getBaseContext(), Patient_main.class);
//    				i1.putExtra("patientID", id);
//    				i1.putExtra("docID",doc_id);
//    				startActivity(i1);
//    				break;
				
                case 1:
                	Intent i2 = new Intent(getBaseContext(), Check_history.class);
    				i2.putExtra("patientID", id);
    				i2.putExtra("docID",doc_id);
    				startActivity(i2);
                    break;
                    
                case 2:
                	Intent i3 = new Intent(getBaseContext(), View_Medicine.class);
    				i3.putExtra("patientID", id);
    				i3.putExtra("docID",doc_id);
    				startActivity(i3);
                    break;
                    
                case 3:
                	Intent i4 = new Intent(getBaseContext(), View_Tests.class);
    				i4.putExtra("patientID", id);
    				i4.putExtra("docID",doc_id);
    				startActivity(i4);
                    break;
                    
                case 4:
					Intent i6 = new Intent(getApplicationContext(), Prescribe_med.class);
					i6.putExtra("patientID", id);
					i6.putExtra("docID",doc_id);
					startActivity(i6);
					finish();
					break;
					
                case 5:
                	Intent i5 = new Intent(getBaseContext(), Prescribe_lab.class);
    				i5.putExtra("patientID", id);
    				i5.putExtra("docID",doc_id);
    				startActivity(i5);
                    break;
                
                case 6:
					Intent i7 = new Intent(getBaseContext(), Conduct_Exam.class);
					i7.putExtra("patientID", id);
					i7.putExtra("docID",doc_id);
					startActivity(i7);
					finish();
					break;
				
                case 7:
					Intent i8 = new Intent(getBaseContext(), Archive.class);
					i8.putExtra("patientID", id);
					i8.putExtra("docID",doc_id);
					startActivity(i8);
					finish();
					break;
                }
                //return super.onOptionsItemSelected(itemPosition);
                return true;
            }
		};
		
		getActionBar().setListNavigationCallbacks(adapter, navigationListener);
		
		
//		Button premExam = (Button) findViewById(R.id.prel_exam);
//		premExam.setVisibility(View.GONE);
//		
//		Button add_med = (Button) findViewById(R.id.add_med);
//		add_med.setVisibility(View.GONE);
//		
//		Button history = (Button)findViewById(R.id.history);
//		history.setVisibility(View.GONE);
//		
//		Button ViewMed = (Button)findViewById(R.id.checkMed);
//		ViewMed.setVisibility(View.GONE);
//		
//		Button patient = (Button)findViewById(R.id.patient);
//		patient.setVisibility(View.GONE);
//		
//		Button logout = (Button)findViewById(R.id.logout);
//		logout.setVisibility(View.GONE);
		
//		TextView pid = (TextView)findViewById(R.id.idP);
//		final String id = getIntent().getExtras().getString("patientID");
//		final String dName = getIntent().getExtras().getString("Name");
//		final String doc_id = getIntent().getExtras().getString("docID");
//		pid.setText(id);
		
		StrictMode.setThreadPolicy(policy); 
		
//		premExam.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				
//				Intent i = new Intent(getBaseContext(), Conduct_Exam.class);
//				i.putExtra("patientID", id);
//				i.putExtra("docID", doc_id);
//				startActivity(i);		
//			}
//		});
//		
//		add_med.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				
//				Intent i = new Intent(getBaseContext(), Prescribe_med.class);
//				i.putExtra("patientID", id);
//				i.putExtra("docID", doc_id);
//				startActivity(i);
//			}
//		});
//		
//		history.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				
//				Intent i = new Intent(getBaseContext(), Check_history.class);
//				i.putExtra("patientID", id);
//				i.putExtra("docID", doc_id);
//				startActivity(i);
//			}
//		});
//		
//		ViewMed.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				
//				Intent i = new Intent(getBaseContext(), View_Medicine.class);
//				i.putExtra("patientID", id);
//				i.putExtra("docID",doc_id);
//				startActivity(i);
//			}
//		});
//		
//		patient.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				Intent p = new Intent(getApplicationContext(), PatientAuthentification.class);
//				p.putExtra("docID", doc_id);
//				startActivity(p);
//				finish();
//				
//			}
//		});
//		
//		logout.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				
//				Intent i = new Intent(getBaseContext(), StaffAuthentification.class);
//				startActivity(i);
//				finish();
//			}
//		});
				
				String P_id = id;
				String D_id = doc_id;
				TextView name = (TextView)findViewById(R.id.txtName);
				TextView age = (TextView)findViewById(R.id.txtAge);
				TextView gender = (TextView)findViewById(R.id.txtSex);
				TextView dob = (TextView)findViewById(R.id.txtDOB);
				
				TextView complain = (TextView)findViewById(R.id.txtPresentComplain);
				TextView bp = (TextView)findViewById(R.id.txtBloodPressure);
				TextView temp = (TextView)findViewById(R.id.txtTemperature);
				TextView pulse = (TextView)findViewById(R.id.txtPulse);
				TextView bmi = (TextView)findViewById(R.id.txtBodyMassIndex);
				TextView treated = (TextView)findViewById(R.id.txtDocID);
				TextView on = (TextView)findViewById(R.id.txtOn);
				
				String result = null;
				InputStream is = null;
				
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("patient_id",P_id));
				
				try{
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://shc.coolpage.biz/fypFinal/fetchpatient_record.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpclient.execute(httppost); 
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();

                    Log.e("log_tag", "connection success ");
                }
            catch(Exception e){
                    Log.e("log_tag", "Error in http connection "+e.toString());
                    Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();
                }
				
				//convert response to string
                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) 
                    {
                            sb.append(line + "\n");
                            
                    }
                    is.close();

                    result=sb.toString();
                }
                catch(Exception e)
                {
                   Log.e("log_tag", "Error converting result "+e.toString());

                Toast.makeText(getApplicationContext(), " Input reading fail", Toast.LENGTH_SHORT).show();
                }
                
              //parse json data
                try{
					JSONObject object = new JSONObject(result);
                    String ch=object.getString("re");
                    if(ch.equals("success")){
						JSONObject no = object.getJSONObject("0");

                        String w= no.getString("pGender");
                        int e=no.getInt("pAge");
                        gender.setText(w);
                        String myString = NumberFormat.getInstance().format(e);
                        age.setText(myString);
                        
                        //Date now = new Date();
                        //Date alsoNow = Calendar.getInstance().getTime();
                        //String nowAsString = new SimpleDateFormat("yyyy-MM-dd").format(now);
                        
                        w=no.getString("visit_time");
                        dob.setText(w);
                        
                        w=no.getString("visit_time");
                        on.setText(w);
                        
                        
                        w=no.getString("Doctor_id");
                        treated.setText(w);
                        
                        w=no.getString("pFName");
                        name.setText(w);
                        
                        w=no.getString("complain");
                        complain.setText(w);
                        
                        w = no.getString("BP");
                        bp.setText(w);
                        
                        w = no.getString("temperature");
                        temp.setText(w);
                        
                        w = no.getString("bmi");
                        bmi.setText(w);
                        
                        w = no.getString("pulse");
                        pulse.setText(w);
                        }
                    else{
                        Toast.makeText(getApplicationContext(), "Record is not available.. Enter valid number", Toast.LENGTH_SHORT).show();
                        }
                }
                catch(JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
                }
				
			}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		try {
			dr = (MenuItem) menu.findItem(R.id.home);
			dr.setTitle(doc_id);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){

		case R.id.dr_logout:
			dr_logout();
			break;
		case R.id.p_logout:
			p_logout();
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void p_logout(){
		Intent btnInt = new Intent(getApplicationContext() , PatientAuthentification.class);
		btnInt.putExtra("docID", doc_id);
		startActivity(btnInt);
		finish();
//		Toast.makeText(getApplicationContext(), "p", Toast.LENGTH_SHORT).show();
	}

	private void dr_logout(){
		//Toast.makeText(getApplicationContext(), "dr_logout", Toast.LENGTH_SHORT).show();
		Intent intStaff = new Intent(getApplicationContext(), StaffAuthentification.class);
		startActivity(intStaff);
		finish();
	}
}
