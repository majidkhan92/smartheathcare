package com.example.healthco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PatientAuthentification extends Activity {

	TextView textView1;
	Button btn;
	EditText txtView;
	AESCrypt AES = new AESCrypt();
	byte[] decryptedPassword = null;
	String dcrypt;
	//Declaration for NFC Activity
	NFCForegroundUtil nfcForegroundUtil;
	int message_limit_classic;
	int message_limit_ultralight;
	int cSec;
	int cBlock;
	boolean nfcAsker;
	
	String doctor_id;
	MenuItem dr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_authentification);

		txtView = (EditText) findViewById( R.id.edtLabtest );
		textView1 = (TextView)findViewById(R.id.txtP);
		btn = (Button) findViewById(R.id.btnScanPid);

		//initialize NFC activity
		nfcForegroundUtil = new NFCForegroundUtil(this);
		message_limit_classic = 144;
		message_limit_ultralight = 48;
		nfcAsker = false;

		doctor_id = getIntent().getExtras().getString("docID");

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		//bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#d21c1c")));

//		btn.setEnabled(false);

		btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				String result = null;
				InputStream is = null;
				String id = txtView.getText().toString();

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("patient_id",id));

				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://shc.coolpage.biz/aadildata/patient_login.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.ISO_8859_1));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					is = entity.getContent();

					Log.e("log_tag", "connection success ");
				}
				catch(Exception e){
					Log.e("log_tag", "Error in http connection "+e.toString());
					Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();
				}

				//convert response to string
				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) 
					{
						sb.append(line + "\n");

					}
					is.close();

					result=sb.toString();
				}
				catch(Exception e)
				{
					Log.e("log_tag", "Error converting result "+e.toString());

					Toast.makeText(getApplicationContext(), " Input reading fail", Toast.LENGTH_SHORT).show();
				}

				//parse json data
				try{
					JSONObject object = new JSONObject(result);
					String ch=object.getString("re");
					if(ch.equals("success")){
						Toast.makeText(getApplicationContext(), "Patient ID Found", Toast.LENGTH_SHORT).show();

						Intent btnInt = new Intent(getApplicationContext() , Patient_main.class);
						btnInt.putExtra("patientID", id);
						btnInt.putExtra("docID", doctor_id);
						startActivity(btnInt);
						finish();
					}

					else{
						Toast.makeText(getApplicationContext(), "Invalid Tag", Toast.LENGTH_SHORT).show();
					}
				}
				catch(JSONException e){
					Log.e("log_tag", "Error parsing data "+e.toString());
					Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	protected void onNewIntent( Intent intent ) {
		//super.onNewIntent( intent );
		//onInit( intent );
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

		MifareUltralight mfc = MifareUltralight.get(tag);
		byte[] data;
		String dec = null ;

		try {       //  5.1) Connect to card 
			mfc.connect();
			boolean auth = false;
			String cardData = null;

			// 5.2) and get the number of sectors this card has..and loop thru these sectors
			int secCount = 12;//mfc.getSectorCount();
			//int bCount = 0;
			//int bIndex = 0;
			for(int j = 5; j <= secCount; j= j+4)
			{

				//read 4 pages at a time
				data = mfc.readPages(j);
				String d = new String(data, "US-ASCII");
				dec = dec + d;
				dec = dec.trim();
			}
		}catch (IOException e) {
		}
		try {
			dec = dec.replace("nullshc*", "");
			dec = dec.replace("null", "");
			decryptedPassword = AES.decrypt(dec.trim());
			dcrypt = new String(decryptedPassword, "US-ASCII");
			txtView.setText(dcrypt);
			btn.setEnabled(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void alertbox(String title, String mymessage)
	{
		new AlertDialog.Builder(this)
		.setMessage("Invalid Tag")
		.setTitle("Invalid Tag")
		.setCancelable(true)
		.show();
	}

	public static class AESCrypt {

		//declarations
		private String iv = "fedcba9876543210";//Dummy iv
		private IvParameterSpec ivspec;
		private SecretKeySpec keyspec;
		private Cipher cipher;

		//128 bit AES key
		private String SecretKey = "*MoNkeY D lUfFy*";//Dummy secretKey => assumption key already exchanged between two parties

		//constructor
		public AESCrypt() {
			ivspec = new IvParameterSpec(iv.getBytes());

			keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES");

			try {
				cipher = Cipher.getInstance("AES/CBC/NoPadding");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//decryption
		public byte[] decrypt(String code) throws Exception {
			if(code == null || code.length() == 0)
				throw new Exception("Empty string");

			byte[] decrypted = null;

			try {
				cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

				decrypted = cipher.doFinal(hexToBytes(code));
			} catch (Exception e) {
				throw new Exception("[decrypt] " + e.getMessage());
			}

			return decrypted;
		}

		//hex to bytes            
		public static byte[] hexToBytes(String str) {
			if (str==null)
			{
				return null;
			} 
			else if (str.length() < 2) 
			{
				return null;
			} 
			else
			{
				int len = str.length() / 2;
				byte[] buffer = new byte[len];

				for (int i=0; i<len; i++)
				{
					buffer[i] = (byte) Integer.parseInt(str.substring(i*2,i*2+2),16);
				}

				return buffer;
			}
		}
	}

	//Called when an activity is going into the background, but has not (yet) been killed
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		nfcForegroundUtil.disableForeground();
	}

	//The activity is in front of all other activities and interacting with the user
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		nfcForegroundUtil.enableForeground();

		//Check if NFC is enabled
		if((!nfcForegroundUtil.getNfc().isEnabled()) && (!nfcAsker))
		{
			//Toast to enable then back to app
			Toast.makeText(getApplicationContext(), 
					"NFC not enabled :(", 
					Toast.LENGTH_LONG).show();
			//NFC Toggler
			startActivity(new Intent(Settings.ACTION_NFC_SETTINGS)); //android.provider.Settings
			//Change boolean
			nfcAsker = true;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_main, menu);
		try {
			dr = (MenuItem) menu.findItem(R.id.phome);
			dr.setTitle(doctor_id);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){

		case R.id.action_logout:
			action_logout();
			break;
		default:
			return super.onOptionsItemSelected(item);
		}

		return true;
	}

	private void action_logout(){

		Intent intStaff = new Intent(getApplicationContext(), StaffAuthentification.class);
		startActivity(intStaff);
		finish();
	}

}
