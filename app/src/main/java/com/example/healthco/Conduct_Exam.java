package com.example.healthco;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Conduct_Exam extends Activity {

	String doc_id;
	MenuItem dr;
	String id;
	/** An array of strings to populate drop down list */
	String[] actions = new String[] {
			"Conduct Preliminary Exam", //conduct exam
			"Patient Profile",	// patient main 1
			"Patient History", 	// check history 2
			"View Medications", // view medicine 3
			"View Tests Prescribed", //view tests 4
			"Prescribe Medicine", // prescribe medicine 5
			"Prescribe Lab Test", // prescribe lab test 6
			"View Full Details", // view archive 7
			
	};


	public void onCreate(Bundle savedInstanceState){

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

		super.onCreate(savedInstanceState);
		setContentView(R.layout.preliminary_exam);

		final String pid = getIntent().getExtras().getString("patientID");
		id = pid;
		final String d_id = getIntent().getExtras().getString("docID");


		/** Create an array adapter to populate drop down list */
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);


		/** Enabling drop down list navigation for the action bar */
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		/** Defining Navigation listener */
		ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition,
					long itemId) {
				Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
				switch (itemPosition) {
				//				case 0:
				//					Intent i2 = new Intent(getBaseContext(), Check_history.class);
				//					i2.putExtra("patientID", id);
				//					i2.putExtra("docID",doc_id);
				//					startActivity(i2);
				//					break;
				//                	
				case 1:
					Intent i1 = new Intent(getBaseContext(), Patient_main.class);
					i1.putExtra("patientID", id);
					i1.putExtra("docID",doc_id);
					startActivity(i1);
					finish();
					break;

				case 2:
					Intent i3 = new Intent(getBaseContext(), Check_history.class);
					i3.putExtra("patientID", id);
					i3.putExtra("docID",doc_id);
					startActivity(i3);
					finish();
					break;
					
				case 3:
					Intent i4 = new Intent(getBaseContext(), View_Medicine.class);
					i4.putExtra("patientID", id);
					i4.putExtra("docID",doc_id);
					startActivity(i4);
					finish();
					break;
					
				case 4:
					Intent i6 = new Intent(getApplicationContext(), View_Tests.class);
					i6.putExtra("patientID", id);
					i6.putExtra("docID",doc_id);
					startActivity(i6);
					finish();
					break;
					
				case 5:
					Intent i5 = new Intent(getBaseContext(), Prescribe_med.class);
					i5.putExtra("patientID", id);
					i5.putExtra("docID",doc_id);
					startActivity(i5);
					finish();
					break;
					
				case 6:
					Intent i7 = new Intent(getBaseContext(), Prescribe_lab.class);
					i7.putExtra("patientID", id);
					i7.putExtra("docID",doc_id);
					startActivity(i7);
					finish();
					break;
				
				case 7:
					Intent i8 = new Intent(getBaseContext(), Archive.class);
					i8.putExtra("patientID", id);
					i8.putExtra("docID",doc_id);
					startActivity(i8);
					finish();
					break;
					
					
				}
				// super.onOptionsItemSelected(itemPosition);
				return true;
			}
		};

		getActionBar().setListNavigationCallbacks(adapter, navigationListener);


		TextView docID = (TextView) findViewById (R.id.editDocID);
		TextView patientID = (TextView) findViewById(R.id.editPName);

		docID.setText(d_id);
		patientID.setText(pid);

		StrictMode.setThreadPolicy(policy); 

		Button submit = (Button) findViewById(R.id.submit_prel);
		submit.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{
				String result = null;
				InputStream is = null;

				String v1 = d_id;

				String v2 = pid;
				EditText complain = (EditText)findViewById(R.id.editPresentComplain);
				String v3 = complain.getText().toString(); 
				EditText bp = (EditText)findViewById(R.id.editBloodPressure);
				String v4 = bp.getText().toString();
				EditText temp = (EditText)findViewById(R.id.editTemperature);
				String v5 = temp.getText().toString();
				EditText pulse = (EditText)findViewById(R.id.editPulse);
				String v6 = pulse.getText().toString();
				EditText bmi = (EditText)findViewById(R.id.editBodyMassIndex);
				String v7 = bmi.getText().toString();

				Date now = new Date();
				Date alsoNow = Calendar.getInstance().getTime();
				String nowAsString = new SimpleDateFormat("yyyy-MM-dd").format(now);

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

				nameValuePairs.add(new BasicNameValuePair("Doctor_id",v1));
				nameValuePairs.add(new BasicNameValuePair("Patient_id",v2));
				nameValuePairs.add(new BasicNameValuePair("complain",v3));
				nameValuePairs.add(new BasicNameValuePair("BP",v4));
				nameValuePairs.add(new BasicNameValuePair("temperature",v5));
				nameValuePairs.add(new BasicNameValuePair("pulse",v6));
				nameValuePairs.add(new BasicNameValuePair("bmi",v7));
				nameValuePairs.add(new BasicNameValuePair("visit_time",nowAsString));


				//http post
				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://shc.coolpage.biz/fypFinal/conduct_preExam.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();
					is = entity.getContent();

					Log.e("log_tag", "connection success ");
					Toast.makeText(getApplicationContext(), "pass", Toast.LENGTH_SHORT).show();
				}

				catch(Exception e)
				{
					Log.e("log_tag", "Error in http connection "+e.toString());
					Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

				}
				//convert response to string
				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) 
					{
						sb.append(line + "\n");
						Intent i = new Intent(getBaseContext(),Patient_main.class);
						i.putExtra("patientID", pid);
						i.putExtra("docID", d_id);
						startActivity(i);
					}
					is.close();

					result=sb.toString();
				}
				catch(Exception e)
				{
					Log.e("log_tag", "Error converting result "+e.toString());
				}
				try{
					JSONObject json_data = new JSONObject(result);
					CharSequence w= (CharSequence) json_data.get("re");
					Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show();
				}
				catch(JSONException e)
				{
					Log.e("log_tag", "Error parsing data "+e.toString());
					Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
				}
			}
		});		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		try {
			dr = (MenuItem) menu.findItem(R.id.home);
			dr.setTitle(doc_id);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){

		case R.id.dr_logout:
			dr_logout();
			break;
		case R.id.p_logout:
			p_logout();
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void p_logout(){
		Intent btnInt = new Intent(getApplicationContext() , PatientAuthentification.class);
		btnInt.putExtra("docID", doc_id);
		startActivity(btnInt);
		finish();
	}

	private void dr_logout(){
		Intent intStaff = new Intent(getApplicationContext(), StaffAuthentification.class);
		startActivity(intStaff);
		finish();
	}
}
