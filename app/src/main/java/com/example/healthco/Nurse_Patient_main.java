package com.example.healthco;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Nurse_Patient_main extends Activity {
	
	String doc_id;
	 MenuItem dr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nurse__patient_main);

		final EditText pid = (EditText)findViewById(R.id.idP);
		final String patientID = getIntent().getExtras().getString("patientID");
		pid.setText(patientID);
		pid.setEnabled(false);

		Button premExam = (Button) findViewById(R.id.prel_exam);
		Button search_name = (Button)findViewById(R.id.searchbyname);
		StrictMode.setThreadPolicy(policy); 

		premExam.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Intent i = new Intent(getBaseContext(), PreliminaryExam.class);
				//startActivity(i);

			}
		});

		search_name.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub



				TextView name = (TextView)findViewById(R.id.txtName);
				TextView age = (TextView)findViewById(R.id.txtAge);
				TextView gender = (TextView)findViewById(R.id.txtSex);
				TextView dob = (TextView)findViewById(R.id.txtDOB);

				TextView complain = (TextView)findViewById(R.id.txtPresentComplain);
				TextView bp = (TextView)findViewById(R.id.txtBloodPressure);
				TextView temp = (TextView)findViewById(R.id.txtTemperature);
				TextView pulse = (TextView)findViewById(R.id.txtPulse);
				TextView bmi = (TextView)findViewById(R.id.txtBodyMassIndex);
				TextView history = (TextView)findViewById(R.id.txtPersonalHistory);

				String result = null;
				InputStream is = null;

				String v1 = pid.getText().toString();
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("PName",v1));

				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://shc.coolpage.biz/aadildata/patient_home.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();
					is = entity.getContent();

					Log.e("log_tag", "connection success ");
				}
				catch(Exception e){
					Log.e("log_tag", "Error in http connection "+e.toString());
					Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();
				}

				//convert response to string
				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) 
					{
						sb.append(line + "\n");

					}
					is.close();

					result=sb.toString();
				}
				catch(Exception e)
				{
					Log.e("log_tag", "Error converting result "+e.toString());

					Toast.makeText(getApplicationContext(), " Input reading fail", Toast.LENGTH_SHORT).show();
				}

				//parse json data
				try{
					JSONObject object = new JSONObject(result);
					String ch=object.getString("re");
					if(ch.equals("success")){
						JSONObject no = object.getJSONObject("0");

						String w= no.getString("PGender");
						int e=no.getInt("PAge");
						gender.setText(w);
						String myString = NumberFormat.getInstance().format(e);
						age.setText(myString);

						w=no.getString("admit_date");
						dob.setText(w);

						w=no.getString("complain");
						complain.setText(w);

						w = no.getString("BP");
						bp.setText(w);

						w = no.getString("temperature");
						temp.setText(w);

						w = no.getString("bmi");
						bmi.setText(w);

						w = no.getString("pulse");
						pulse.setText(w);
					}
					else{
						Toast.makeText(getApplicationContext(), "Record is not available.. Enter valid number", Toast.LENGTH_SHORT).show();
					}
				}
				catch(JSONException e){
					Log.e("log_tag", "Error parsing data "+e.toString());
					Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		try {
			dr = (MenuItem) menu.findItem(R.id.home);
			dr.setTitle(doc_id);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){

		case R.id.dr_logout:
			dr_logout();
			break;
		case R.id.p_logout:
			p_logout();
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void p_logout(){
		Intent btnInt = new Intent(getApplicationContext() , PatientAuthentification.class);
		btnInt.putExtra("docID", doc_id);
		startActivity(btnInt);
		finish();
	}

	private void dr_logout(){
		Intent intStaff = new Intent(getApplicationContext(), StaffAuthentification.class);
		startActivity(intStaff);
		finish();
	}
}
