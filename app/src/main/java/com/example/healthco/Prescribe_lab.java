package com.example.healthco;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Prescribe_lab extends Activity {
	
	String doc_id;
	MenuItem dr;
	/** An array of strings to populate drop down list */
	String[] actions = new String[] {
			"Prescribe Lab Test", // prescribe lab test 
			"Patient Profile",	// patient main
			"Patient History", 	// check history
			"View Medications", // view medicine
			"View Tests Prescribed", //view tests 4
			"Prescribe Medicine", // prescribe medicine 5
			"Conduct Preliminary Exam", //conduct exam 6
			"View Full Details", // view archive 7
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		final String id = getIntent().getExtras().getString("patientID");
		final String doc_id = getIntent().getExtras().getString("docID");

		/** Create an array adapter to populate drop down list */
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);


		/** Enabling drop down list navigation for the action bar */
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);


		/** Defining Navigation listener */
		/** Defining Navigation listener */
		ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition,
					long itemId) {
				Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
				switch (itemPosition) {
				case 1:
					Intent i1 = new Intent(getBaseContext(), Patient_main.class);
					i1.putExtra("patientID", id);
					i1.putExtra("docID",doc_id);
					startActivity(i1);
					finish();
					break;	

				case 2:
					Intent i3 = new Intent(getBaseContext(), Check_history.class);
					i3.putExtra("patientID", id);
					i3.putExtra("docID",doc_id);
					startActivity(i3);
					finish();
					break;
					
				case 3:
					Intent i2 = new Intent(getBaseContext(), View_Medicine.class);
					i2.putExtra("patientID", id);
					i2.putExtra("docID",doc_id);
					startActivity(i2);
					finish();
					break;

				case 4:
					Intent i4 = new Intent(getApplicationContext(), View_Tests.class);
					i4.putExtra("patientID", id);
					i4.putExtra("docID",doc_id);
					startActivity(i4);
					finish();
					break;
					
				case 5:
					Intent i5 = new Intent(getBaseContext(), Prescribe_med.class);
					i5.putExtra("patientID", id);
					i5.putExtra("docID",doc_id);
					startActivity(i5);
					finish();
					break;
					
				case 6:
					Intent i7 = new Intent(getBaseContext(), Conduct_Exam.class);
					i7.putExtra("patientID", id);
					i7.putExtra("docID",doc_id);
					startActivity(i7);
					finish();
					break;
				
				case 7:
					Intent i8 = new Intent(getBaseContext(), Archive.class);
					i8.putExtra("patientID", id);
					i8.putExtra("docID",doc_id);
					startActivity(i8);
					finish();
					break;
				}
				// super.onOptionsItemSelected(itemPosition);
				return true;
			}
		};

		getActionBar().setListNavigationCallbacks(adapter, navigationListener);

		
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		setContentView(R.layout.activity_prescribe_lab);
		
		Button submit = (Button) findViewById(R.id.btnSubmit);
		Button add = (Button) findViewById(R.id.btnSubmitAnother);
		
		TextView Pid = (TextView)findViewById(R.id.txtPid);
		Pid.setText(id);

		StrictMode.setThreadPolicy(policy);
		
		submit.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{

				String result = null;
				InputStream is = null;
				EditText lab = (EditText)findViewById(R.id.edtLabtest);
				String v2 = lab.getText().toString();
				String v3 = "";

				Date now = new Date();
				Date alsoNow = Calendar.getInstance().getTime();
				String nowAsString = new SimpleDateFormat("yyyy-MM-dd").format(now);

				String to = "nefhea@hotmail.com";
				String subject = "Lab test for" + id;
				String message = "Patient ID: " + id + "\n" + "Doctor ID: " + doc_id + "\n" + "Prescribe Lab Test: " + v2 + "\n" + nowAsString;

				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
				//email.putExtra(Intent.EXTRA_CC, new String[]{ to});
				//email.putExtra(Intent.EXTRA_BCC, new String[]{to});
				email.putExtra(Intent.EXTRA_SUBJECT, subject);
				email.putExtra(Intent.EXTRA_TEXT, message);

				//need this to prompts email client only
				email.setType("message/rfc822");

				startActivity(Intent.createChooser(email, "Choose an Email client :"));

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

				nameValuePairs.add(new BasicNameValuePair("Patient_id",id));
				nameValuePairs.add(new BasicNameValuePair("Doctor_id",doc_id));
				nameValuePairs.add(new BasicNameValuePair("Lab_Test",v2));
				nameValuePairs.add(new BasicNameValuePair("description",v3));
				nameValuePairs.add(new BasicNameValuePair("prescribed_on",nowAsString));

				//http post
				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://shc.coolpage.biz/fypFinal/add_test.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();
					is = entity.getContent();

					Log.e("log_tag", "connection success ");
					Toast.makeText(getApplicationContext(), "pass", Toast.LENGTH_SHORT).show();
				}


				catch(Exception e)
				{
					Log.e("log_tag", "Error in http connection "+e.toString());
					Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

				}
				//convert response to string
				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) 
					{
						sb.append(line + "\n");
						Intent i = new Intent(getBaseContext(),Patient_main.class);
						i.putExtra("patientID", id);
						i.putExtra("docID", doc_id);
						startActivity(i);
					}
					is.close();

					result=sb.toString();
				}
				catch(Exception e)
				{
					Log.e("log_tag", "Error converting result "+e.toString());
				}

				try{
					JSONObject json_data = new JSONObject(result);
					CharSequence w= (CharSequence) json_data.get("re");
					Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show(); 	      
				}
				catch(JSONException e)
				{
					Log.e("log_tag", "Error parsing data "+e.toString());
					Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
				}
			}
		});

		add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String result = null;
				InputStream is = null;
				EditText lab = (EditText)findViewById(R.id.edtLabtest);
				String v2 = lab.getText().toString();
				String v3 = "";
				
				Date now = new Date();
				Date alsoNow = Calendar.getInstance().getTime();
				String nowAsString = new SimpleDateFormat("yyyy-MM-dd").format(now);

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

				nameValuePairs.add(new BasicNameValuePair("Patient_id",id));
				nameValuePairs.add(new BasicNameValuePair("Doctor_id",doc_id));
				nameValuePairs.add(new BasicNameValuePair("Lab_Test",v2));
				nameValuePairs.add(new BasicNameValuePair("description",v3 ));
				nameValuePairs.add(new BasicNameValuePair("prescribed_on",nowAsString));
				
				String to = "nefhea@hotmail.com";
				String subject = "Lab test for" + id;
				String message = "Patient ID: " + id + "\n" + "Doctor ID: " + doc_id + "\n" + "Prescribe Lab Test: " + v2 + "\n" + nowAsString;

				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
				//email.putExtra(Intent.EXTRA_CC, new String[]{ to});
				//email.putExtra(Intent.EXTRA_BCC, new String[]{to});
				email.putExtra(Intent.EXTRA_SUBJECT, subject);
				email.putExtra(Intent.EXTRA_TEXT, message);


				//http post
				try{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://shc.coolpage.biz/fypFinal/add_test.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();
					is = entity.getContent();

					Log.e("log_tag", "connection success ");
					Toast.makeText(getApplicationContext(), "pass", Toast.LENGTH_SHORT).show();
				}


				catch(Exception e)
				{
					Log.e("log_tag", "Error in http connection "+e.toString());
					Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();

				}
				//convert response to string
				try{
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) 
					{
						sb.append(line + "\n");
						Intent i = new Intent(getBaseContext(),Prescribe_lab.class);
						i.putExtra("patientID", id);
						i.putExtra("docID", doc_id);
						startActivity(i);
					}
					is.close();

					result=sb.toString();
				}
				catch(Exception e)
				{
					Log.e("log_tag", "Error converting result "+e.toString());
				}

				try{
					JSONObject json_data = new JSONObject(result);
					CharSequence w= (CharSequence) json_data.get("re");
					Toast.makeText(getApplicationContext(), w, Toast.LENGTH_SHORT).show(); 	      
				}
				catch(JSONException e)
				{
					Log.e("log_tag", "Error parsing data "+e.toString());
					Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
				}
			}
		});



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		try {
			dr = (MenuItem) menu.findItem(R.id.home);
			dr.setTitle(doc_id);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){

		case R.id.dr_logout:
			dr_logout();
			break;
		case R.id.p_logout:
			p_logout();
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void p_logout(){
		Intent btnInt = new Intent(getApplicationContext() , PatientAuthentification.class);
		btnInt.putExtra("docID", doc_id);
		startActivity(btnInt);
		finish();
	}

	private void dr_logout(){
		Intent intStaff = new Intent(getApplicationContext(), StaffAuthentification.class);
		startActivity(intStaff);
		finish();
	}


}
