package com.example.healthco;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class StaffAuthentification extends Activity {

	public static TextView txtView2;
	public static TextView textView1;
	public static EditText ePinCode;
	public static EditText eUserId;
	public static Button btnVrfy;
	public static String DB_id = "Dr. Saad"; // doctor id
	public static String id = null; // doctor enters id
	public static String Dr_hash1 = null; // doctor enters pin
	public static String pass = null;
	NFCForegroundUtil nfcForegroundUtil;
	NfcAdapter mAdapter ;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staff_authentification);
		btnVrfy = (Button) findViewById(R.id.btnProceed);
		ePinCode = (EditText) findViewById(R.id.edtPinCode);
		eUserId = (EditText) findViewById(R.id.edtUserId);
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		//ePinCode.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		nfcForegroundUtil = new NFCForegroundUtil(this);


		btnVrfy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Dr_hash1 = ePinCode.getText().toString().trim();
				id = eUserId.getText().toString().trim();
				
				String result = null;
				InputStream is = null;

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				
				nameValuePairs.add(new BasicNameValuePair("emp_id",id));
				
				try{
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://shc.coolpage.biz/fypFinal/emp_login.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.ISO_8859_1));
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();

                    Log.e("log_tag", "connection success ");
                }
            catch(Exception e){
                    Log.e("log_tag", "Error in http connection "+e.toString());
                    Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();
                }
				
				 //convert response to string
                try{
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) 
                    {
                            sb.append(line + "\n");
                            
                    }
                    is.close();

                    result=sb.toString();
                }
                catch(Exception e)
                {
                   Log.e("log_tag", "Error converting result "+e.toString());

                Toast.makeText(getApplicationContext(), " Input reading fail", Toast.LENGTH_SHORT).show();
                }
                
              //parse json data
                try{
					JSONObject object = new JSONObject(result);
                    String ch=object.getString("re");
                    if(ch.equals("success")){
						JSONObject no = object.getJSONObject("0");
						
						String name = no.getString("eFName");
                        String w = no.getString("password");
                        String x= no.getString("designation");
                        
                        if(Dr_hash1.equals(w)){
                        	Toast.makeText(getApplicationContext(), "Login Successfull", Toast.LENGTH_SHORT).show();
                        	
//                        	if(x.equals("Doctor")){
//                        		Intent btnInt = new Intent(getApplicationContext(), PatientAuthentification.class);
//                				btnInt.putExtra("docID", id);
//                				btnInt.putExtra("Name", name);
//                				startActivity(btnInt);
//                				finish();
//                        	}
                        	if(x.equals("Doctor")){
                        		Intent btnInt = new Intent(getApplicationContext(), PatientAuthentification.class);
                				btnInt.putExtra("docID", name);
                				startActivity(btnInt);
                				finish();
                        	}
                        	else{
                        		Intent intent = new Intent(getApplicationContext(), Nurse_Patient_main.class);
                        		startActivity(intent);
                        		intent.putExtra("nurseID", id);
                        		intent.putExtra("Name", name);
                        		finish();
                        	}
                        }
                        else{
                        	Toast.makeText(getApplicationContext(), "Invalid Pin Code", Toast.LENGTH_SHORT).show();
                        }
                   }    
                        
                        else{
                        	Toast.makeText(getApplicationContext(), "ID not Registered", Toast.LENGTH_SHORT).show();         	
                        }
                    }
                
                catch(JSONException e){
                        Log.e("log_tag", "Error parsing data "+e.toString());
                        Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
                }	
			}
		});

		}

	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent( intent );
		onInit( intent );
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		nfcForegroundUtil.disableForeground();
		if (!nfcForegroundUtil.getNfc().isEnabled())
		{
			Toast.makeText(getApplicationContext(), 
					"Please activate NFC and press Back to return to the application!", 
					Toast.LENGTH_LONG).show();
			startActivity(
					new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		nfcForegroundUtil.enableForeground();
	}

	public void onInit(Intent intent) {

		String action = intent.getAction();
		//txt.setText(action);

		if(action.equals(NfcAdapter.ACTION_NDEF_DISCOVERED))
		{
			Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			NdefMessage msg = (NdefMessage) rawMsgs[0];
			String host = new String(msg.getRecords()[0].getPayload());
			host = host.replace("en", "");
			//if(host.substring(0, 3) == "shc") {
			try {
				host = host.replace("shc*", "");
				eUserId.setText(host);
				eUserId.setEnabled(false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//}
		}


	}

	public static String getHash(String clearTextPassword)   {  

		byte[] d = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(clearTextPassword.getBytes());
			d= Base64.encode(md.digest(), Base64.DEFAULT);
		} catch (NoSuchAlgorithmException e) {
			//_log.error("Failed to encrypt password.", e);
		}
		return new String(d);
	}


}
