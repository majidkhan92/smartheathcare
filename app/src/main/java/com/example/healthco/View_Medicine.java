package com.example.healthco;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActionBar.OnNavigationListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class View_Medicine extends Activity {

	String doc_id;
	MenuItem dr;
	
	/** An array of strings to populate drop down list */
	String[] actions = new String[] {
		"View Medications", // view medicine
		"Patient Profile",	// patient main 1
		"Patient History", 	// check history 2
		"View Tests Prescribed", //view tests 3 
		"Prescribe Medicine", // prescribe medicine 4
		"Prescribe Lab Test", // prescribe lab test 5
		"Conduct Preliminary Exam", //conduct exam 6
		"View Full Details", // view archive 7
	};
	

	public void onCreate(Bundle savedInstanceState){

		String returnString;

		super.onCreate(savedInstanceState);
		setContentView(R.layout.exam_history);

		final String id = getIntent().getExtras().getString("patientID");
		final String dName = getIntent().getExtras().getString("Name");
		final String doc_id = getIntent().getExtras().getString("docID");

		String P_id = id;
		String D_id = doc_id;
		TextView history = (TextView)findViewById(R.id.tvHistory);

		String result = null;
		InputStream is = null;

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("patientID",P_id));
		
		/** Create an array adapter to populate drop down list */
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, actions);


		/** Enabling drop down list navigation for the action bar */
		getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		
		/** Defining Navigation listener */
		ActionBar.OnNavigationListener navigationListener = new OnNavigationListener() {

			@Override
			public boolean onNavigationItemSelected(int itemPosition,
					long itemId) {
				Toast.makeText(getBaseContext(), "You selected : " + actions[itemPosition]  , Toast.LENGTH_SHORT).show();
				switch (itemPosition) {
				case 1:
					Intent i1 = new Intent(getBaseContext(), Patient_main.class);
    				i1.putExtra("patientID", id);
    				i1.putExtra("docID",doc_id);
    				startActivity(i1);
    				finish();
					break;
					
				case 2:
					Intent i2 = new Intent(getBaseContext(), Check_history.class);
					i2.putExtra("patientID", id);
					i2.putExtra("docID",doc_id);
					startActivity(i2);
					finish();
					break;
					
				case 3:
					Intent i4 = new Intent(getBaseContext(), View_Tests.class);
					i4.putExtra("patientID", id);
					i4.putExtra("docID",doc_id);
					startActivity(i4);
					finish();
					break;
					
				case 4:
					Intent i6 = new Intent(getApplicationContext(), Prescribe_med.class);
					i6.putExtra("patientID", id);
					i6.putExtra("docID",doc_id);
					startActivity(i6);
					finish();
					break;
					
				case 5:
					Intent i5 = new Intent(getBaseContext(), Prescribe_lab.class);
					i5.putExtra("patientID", id);
					i5.putExtra("docID",doc_id);
					startActivity(i5);
					finish();
					break;
					
				case 6:
					Intent i7 = new Intent(getBaseContext(), Conduct_Exam.class);
					i7.putExtra("patientID", id);
					i7.putExtra("docID",doc_id);
					startActivity(i7);
					finish();
					break;
					
				case 7:
					Intent i8 = new Intent(getBaseContext(), Archive.class);
					i8.putExtra("patientID", id);
					i8.putExtra("docID",doc_id);
					startActivity(i8);
					finish();
					break;
					
				}
				// super.onOptionsItemSelected(itemPosition);
				return true;
			}
		};
		
		getActionBar().setListNavigationCallbacks(adapter, navigationListener);

		try{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://shc.coolpage.biz/fypFinal/fetch_med_history.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost); 
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

			Log.e("log_tag", "connection success ");
		}
		catch(Exception e){
			Log.e("log_tag", "Error in http connection "+e.toString());
			Toast.makeText(getApplicationContext(), "Connection fail", Toast.LENGTH_SHORT).show();
		}

		//convert response to string
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) 
			{
				sb.append(line + "\n");

			}
			is.close();

			result=sb.toString();
		}
		catch(Exception e)
		{
			Log.e("log_tag", "Error converting result "+e.toString());

			Toast.makeText(getApplicationContext(), " Input reading fail", Toast.LENGTH_SHORT).show();
		}

		//parse json data
		try{	
			returnString = "";
			JSONArray jArray = new JSONArray(result);
			for(int i=0;i<jArray.length();i++){
	        	JSONObject json_data = jArray.getJSONObject(i);
	        	Log.i("log_tag","Patient Id: "+json_data.getString("patientID")+
	        			", Doctor ID: "+json_data.getString("doctorID")+
	        			", Medicine: "+json_data.getString("medicine")+
	        			", Description: "+json_data.getString("description")+
	        			", Prescribed On: "+json_data.getString("prescribed_on")
	            );
	        
	        returnString += "\n" + " Prescribed On: "+ json_data.getString("prescribed_on") + " \n "
	        		+ " Prescription: " + json_data.getString("medicine") + " \n "
	        		+ " Prescribed By: "+ json_data.getString("doctorID")+ " \n "
	        		+ " Description: "+ json_data.getString("description")
	        		+ " \n "
	        		+ " \n ";	
	        }

			try{
				history.setText(returnString);
			}
			catch(Exception e){
				Log.e("log_tag","Error in Display!" + e.toString());;          
			}   


		}
		catch(JSONException e){
			Log.e("log_tag", "Error parsing data "+e.toString());
			Toast.makeText(getApplicationContext(), "JsonArray fail", Toast.LENGTH_SHORT).show();
		}


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		try {
			dr = (MenuItem) menu.findItem(R.id.home);
			dr.setTitle(doc_id);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){

		case R.id.dr_logout:
			dr_logout();
			break;
		case R.id.p_logout:
			p_logout();
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	private void p_logout(){
		Intent btnInt = new Intent(getApplicationContext() , PatientAuthentification.class);
		btnInt.putExtra("docID", doc_id);
		startActivity(btnInt);
		finish();
	}

	private void dr_logout(){
		Intent intStaff = new Intent(getApplicationContext(), StaffAuthentification.class);
		startActivity(intStaff);
		finish();
	}
}
